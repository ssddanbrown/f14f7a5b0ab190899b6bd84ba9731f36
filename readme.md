This is a hack to BookStack, using the theme system, to email users about page updates to the pages they have favourited. 

### Setup

This uses the [logical theme system](https://github.com/BookStackApp/BookStack/blob/development/dev/docs/logical-theme-system.md).

1. Within the BookStack install folder, you should have a `themes` folder.
2. Create a `themes/custom/functions.php` file with the contents of the `functions.php` file example below.
3. Customize the email message, if desired, by editing the lines of text within the `toMail` part at around lines 32-35.
4. Add `APP_THEME=custom` to your .env file.

### Note

The sending of emails may slow down page update actions, and these could be noisy if a user edits a page many times quickly. 
These customizations are not officially supported any may break upon, or conflict with, future updates. 
Quickly tested on BookStack v22.11.